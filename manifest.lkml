project_name: "accure-custom-visualizations"


constant: plotly {
  value: "https://cdn.plot.ly/plotly-1.57.1.min.js"
}


visualization: {
  id: "acv-simple_vis"
  label: "ACV: Simple React Visualization"
  # dependencies: ["https://cdn.plot.ly/plotly-1.57.1.min.js"]
  dependencies: ["@{plotly}"]
  file: "dist/simple_vis.js"
}

visualization: {
  id: "acv-repeater"
  label: "ACV: Repeater"
  dependencies: []
  file: "dist/repeater.js"
}

visualization: {
  id: "acv-local-dev"
  label: "ACV: Local Developer"
  dependencies: []
  url:"http://localhost:8000/simple_vis.js"
}

visualization: {
  id: "acv-timeseries-vis"
  label: "ACV: Timeseries"
  dependencies: ["@{plotly}"]
  file: "dist/timeseries_vis.js"
}


visualization: {
  id: "acv-heatmap-vis"
  label: "ACV: Heatmap"
  dependencies: ["@{plotly}"]
  file: "dist/heatmap_vis.js"
}

visualization: {
  id: "acv-heatmap_api-vis"
  label: "ACV: Heatmap API"
  dependencies: ["/src/common/utils.js", "/src/common/d3.v4.js"]
  file: "src/heatmap_api_vis/matrix.js"
}

visualization: {
  id: "acv-mg_timeseries_vis"
  label: "ACV: HaoMing Timeseries Vis"
  dependencies: ["https://cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.min.js"]
  file: "dist/mg_timeseries_vis.js"
}
