import TimeSeriesPlot from './TimeseriesComponent'
import Loading from '../common/ReactComponents/Loading'
import React from 'react'
import ReactDOM from 'react-dom'

looker.plugins.visualizations.add({

  options: {
    font_size: {
      type: "string",
      label: "Font Size",
      values: [
        { "Large": "large" },
        { "Small": "small" }
      ],
      display: "radio",
      default: "large"
    }
  },
  // Set up the initial state of the visualization
  create: function (element, config) {

    // Insert a <style> tag with some styles we'll use later.
    element.innerHTML = `
      <style>
        .vis-main {
          /* Vertical centering */
          height: 100%;
          display: flex;
          flex-direction: column;
          justify-content: center;
          text-align: center;
        }
        .text-large {
          font-size: 72px;
        }
        .text-small {
          font-size: 18px;
        }
      </style>
    `;

    // Create a container element to let us center the text.
    let container = element.appendChild(document.createElement("div"));
    container.className = "vis-main";

    // Create an element to contain the text.
    this._textElement = container.appendChild(document.createElement("div"));

    // Render to the target element
    this.chart = ReactDOM.render(
      <Loading />,
      this._textElement
    );

  },
  // Render in response to the data or settings changing
  updateAsync: function (data, element, config, queryResponse, details, done) {
    // Clear any errors from previous updates
    this.clearErrors();

    // Throw some errors and exit if the shape of the data isn't what this chart needs
    if (queryResponse.fields.dimensions.length == 0) {
      this.addError({ title: "No Dimensions", message: "This chart requires dimensions." });
      return;
    }

    // Set the size to the user-selected size
    if (config.font_size == "small") {
      this._textElement.className = "text-small";
    } else {
      this._textElement.className = "text-large";
    }

    // Unpack data
    let time_data = data.map(function(item){
        let output = {
            time_column: item[config.query_fields.dimensions[0].name]["value"]
        };

        for ( let i=1; i<config.query_fields.dimensions.length; i++){

            let value = item[config.query_fields.dimensions[i].name]["value"];
            let label = config.query_fields.dimensions[i].label_short;

            output[label] = value;
        }

        return output
    });

    const [time_column, ...traces] = Object.keys(time_data[0]);

    // Finally update the state with our new data
    this.chart = ReactDOM.render(
      <TimeSeriesPlot 
        data={time_data} 
        traces={traces} 
        start_time={time_data[0]['time_column']} 
        end_time={time_data[time_data.length-1]['time_column']}/>,
      this._textElement
    );

    // We are done rendering! Let Looker know.
    done()
  }
});
