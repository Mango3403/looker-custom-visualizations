import React, { useState, useEffect } from 'react';

// Create (or import) our react component
export default class TimeSeriesPlot extends React.Component {
  constructor(props) {
    // So we have access to 'this'
    super(props)
  }

  buildPlot() {

    console.log({ traces: this.props.traces });

    function unpack(rows, key) {
      return rows.map(function (row) { return row[key]; });
    }

    let data = [];
    let colors = ['#36C799', 'red', 'blue', 'purple'];

    let trace_idx = 0;
    for (const name of this.props.traces) {
      data.push({
        type: "scatter",
        mode: "lines",
        name: name,
        x: unpack(this.props.data, 'time_column'),
        y: unpack(this.props.data, name),
        line: { color: colors[trace_idx] }
      });

      trace_idx++;
    }

    var layout = {
      title: 'Time Series with Rangeslider',
      xaxis: {
        autorange: true,
        range: [this.props.start_time, this.props.end_time],
        rangeselector: {
          buttons: [
            {
              count: 1,
              label: '1m',
              step: 'month',
              stepmode: 'backward'
            },
            {
              count: 6,
              label: '6m',
              step: 'month',
              stepmode: 'backward'
            },
            { step: 'all' }
          ]
        },
        rangeslider: { range: [this.props.start_time, this.props.end_time] },
        type: 'date'
      },
      yaxis: {
        autorange: true,
        // range: [-100, 100],
        type: 'linear'
      },
      dragmode: "pan"

    };

    Plotly.newPlot('myDiv', data, layout, { scrollZoom: true });
  }

  componentDidMount() {
    this.buildPlot();
  }

  componentDidUpdate() {
    this.buildPlot();
  }

  // render our data
  render() {
    return <div id="myDiv"></div>;
  }
}
