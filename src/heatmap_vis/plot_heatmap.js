import Heatmap from './HeatmapComponents'
import Loading from '../common/ReactComponents/Loading'
import React from 'react'
import ReactDOM from 'react-dom'

looker.plugins.visualizations.add({

  options: {
    font_size: {
      type: "string",
      label: "Font Size",
      values: [
        { "Large": "large" },
        { "Small": "small" }
      ],
      display: "radio",
      default: "large"
    }
  },
  // Set up the initial state of the visualization
  create: function (element, config) {

    // Insert a <style> tag with some styles we'll use later.
    element.innerHTML = `
      <style>
        .vis-main {
          /* Vertical centering */
          height: 100%;
          display: flex;
          flex-direction: column;
          justify-content: center;
          text-align: center;
        }
        .text-large {
          font-size: 72px;
        }
        .text-small {
          font-size: 18px;
        }
      </style>
    `;

    // Create a container element to let us center the text.
    let container = element.appendChild(document.createElement("div"));
    container.className = "vis-main";

    // Create an element to contain the text.
    this._textElement = container.appendChild(document.createElement("div"));

    // Render to the target element
    this.chart = ReactDOM.render(
      <Loading />,
      this._textElement
    );

  },
  // Render in response to the data or settings changing
  updateAsync: function (data, element, config, queryResponse, details, done) {

    // Clear any errors from previous updates
    this.clearErrors();

    // Throw some errors and exit if the shape of the data isn't what this chart needs
    if (queryResponse.fields.dimensions.length == 0) {
      this.addError({ title: "No Dimensions", message: "This chart requires dimensions." });
      return;
    }

    // Set the size to the user-selected size
    if (config.font_size == "small") {
      this._textElement.className = "text-small";
    } else {
      this._textElement.className = "text-large";
    }

    // Unpack data
    let data_z = [];
    let data_url = [];
    let row_labels = [];

    let column_labels = config.query_fields.pivots[0].enumerations.map((item) => { return item.label });
    let columns = config.query_fields.pivots[0].enumerations.map((item, i) => { return `${item.value}|FIELD|${i.toString().padStart(2, "0")}` });

    data.map(function (item) {
      let row_label = item[config.query_fields.dimensions[0].name].value;
      row_labels.push(row_label);

      let measure_name = config.query_fields.measures[0].name;

      let row_data = [];
      let row_urls = [];

      for (const col of columns) {
        let subitem = item[measure_name][col];
        if (subitem !== undefined) {
          row_data.push(subitem.value);

          if (subitem.links !== undefined) {
            row_urls.push(subitem.links[1].url); // Get drill url
          } else {
            row_urls.push(null);
          }

        } else {
          row_data.push(null);
          row_urls.push(null);
        }
      }

      data_z.push(row_data);
      data_url.push(row_urls)
    });


    // let row_labels = config.query_fields.dimensions[0].enumerations.map((item) => { return item.label });

    // console.log({ data_z });
    // console.log({ column_labels });
    // console.log({ row_labels });
    // const [time_column, ...traces] = Object.keys(time_data[0]);

    // Finally update the state with our new data
    this.chart = ReactDOM.render(
      <Heatmap
        data={data_z}
        urls={data_url}
        column_labels={column_labels}
        row_labels={row_labels}
      />,
      this._textElement
    );

    // We are done rendering! Let Looker know.
    done()
  }
});