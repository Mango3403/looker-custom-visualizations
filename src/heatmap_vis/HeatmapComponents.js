import React, { useState, useEffect } from 'react';

// Create (or import) our react component
export default class Heatmap extends React.Component {
  constructor(props) {
    // So we have access to 'this'
    super(props)
  }

  buildPlot() {
    var data = [
      {
        z: this.props.data,
        x: this.props.column_labels,
        y: this.props.row_labels,
        urls: this.props.urls,
        type: 'heatmap',
        hoverongaps: false
      }
    ];

    Plotly.newPlot('myDiv', data);

    var myPlot = document.getElementById('myDiv');
    myPlot.on("plotly_click", (event) => {
      let idx = event.points[0].pointIndex;
      let url = event.points[0].data.urls[idx[0]][idx[1]];

      if (url !== undefined && url !== null) {
        console.log(`Going to ${url}`);
        window.open(url); // Go to drill down url
      }

    });
  }

  componentDidMount() {
    this.buildPlot();
  }

  componentDidUpdate() {
    this.buildPlot();
  }

  // render our data
  render() {
    return <div id="myDiv"></div>;
  }
}
