import React from 'react';
import ReactDOM from 'react-dom';
import HelloComponent from './HelloComponent';

looker.plugins.visualizations.add({
	create: function(element, config){
		// element.innerHTML = "<h1>Ready to render!</h1>";
		
		this.container = element.appendChild(document.createElement('div'));
		this.container.className = 'vis-main';
	},
	updateAsync: function(data, element, config, queryResponse, details, doneRendering){
		// Clear errors from previous updates
		this.clearErrors();

		let d = ['a', 'b', 'c'];

		// Update component
		ReactDOM.render(<HelloComponent data={d} />, this.container);

		// Done render
		doneRendering();
	}
});