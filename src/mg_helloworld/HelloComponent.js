import React, { useState, useEffect, useRef } from 'react';
import D3Component from './d3-component.js';

let vis = null; // independent d3 component

export default function MyVisComponent({ data }) {
  const [width, setWidth] = useState(600);
  const [height, setHeight] = useState(600);
  const [active, setActive] = useState(null);
  const refElement = useRef(null);

  // useEffect(handleResizeEvent, []);
  useEffect(initVis, [data]);
  useEffect(() => {
    vis && vis.resize(width, height);
  }, [width, height]);

  function handleResizeEvent() {
    let resizeTimer;
    const handleResize = () => {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function () {
        setWidth(window.innerWidth);
        setHeight(window.innerHeight);
      }, 300);
    };
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }

  function initVis() {
    if (data && data.length) {
      const d3Props = {
        data,
        width,
        height,
        onDatapointClick: setActive
      };
      vis = new D3Component(refElement.current, d3Props);
    }
  }

  function updateVisOnResize() {
    vis && vis.resize(width, height);
  }

  return (
    <>
      <div ref={refElement} />
      <div>{active}</div>
    </>
  );
}