/**
 * dataset sample:
 * @typedef {{ date: Date, [value0]: (string|number), [value1]: (string|number), ... }} Data
 * @typedef {Data[]} Dataset
 */

import ReactDOM from 'react-dom';
import React from 'react';
import Loading from '../common/ReactComponents/Loading'
import Chart from './ChartComponent';
// import { fetchData } from './common/util';

looker.plugins.visualizations.add({
  options: {
  },
  create: function (element, config) {

    // style tag
    element.innerHTML = `
      <style>
        .vis-main {
        }
        svg.d3c {
          font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif;
          font-size: 12px;
        }
        .d3c-zoom {
          cursor: move;
        }
        .d3c-focus .d3c-axis--x .domain,
        .d3c-focus .d3c-axis--y .domain {
          stroke-width: 0;
        }
      </style>
    `

    // Create a container element
    this._container = element.appendChild(document.createElement("div"));
    this._container.className = 'vis-main'

    // Render to the target element
    this.chart = ReactDOM.render(
      <Loading />,
      this._container
    );
  },
  updateAsync: function (data, element, config, queryResponse, details, done) {
    // Clear any error from previous updates
    this.clearErrors();

    // Throw erros
    if (queryResponse.fields.dimensions.length == 0) {
      this.addError({ title: "No Dimensions", message: "This chart requires dimensions." });
      return;
    }

    // RESTful method
    // const event_file = 'event_1';
    // fetchData(event_file).then((dataset) => {
    //   // console.log('dataset', dataset);
    //   this.chart = ReactDOM.render(
    //     <>
    //       <h2>D3 v6</h2>
    //       <Chart
    //         event_file={event_file}
    //         data={dataset}
    //         start_time={dataset[dataset.length - 1]['date']}
    //         end_time={dataset[0]['date']}
    //       />
    //     </>,
    //     this._container
    //   );
    //   // Done render
    //   done();
    // })

    let parse_data = ({ data, config }) => {
      return data.map(function (item) {
        let output = {
          time_column: item[config.query_fields.dimensions[0].name]["value"]
        };

        for (let i = 1; i < config.query_fields.dimensions.length; i++) {

          let value = item[config.query_fields.dimensions[i].name]["value"];
          let label = config.query_fields.dimensions[i].label_short;

          output[label] = value;
        }

        return output;
      });
    };
    let parse_time = d3.timeParse("%Y-%m-%d %H:%M:%S");
    const parsed_data = parse_data({ data, config });
    console.log('parsed_data', parsed_data);
    const [time_column, ...traces] = Object.keys(parsed_data[0]);

    /** @type {Dataset} */
    let dataset = [];
    if (traces.length > 0) {
      dataset = parsed_data.map(d => {
        return traces.reduce(
          (data, key, idx) => ({ ...data, ['value' + idx]: d[key] }),
          { date: parse_time(d['time_column']) }
        );
      });
    } else {
      dataset = parsed_data.map(d => ({ date: parse_time(d['time_column']) }))
    }


    console.log(dataset);
    this.chart = ReactDOM.render(
      <Chart
        title={"D3 v6"}
        subTitle={"Source: Looker"}
        data={dataset}
        start_time={dataset[dataset.length - 1]['date']}
        end_time={dataset[0]['date']}
      />,
      this._container
    );
    // Done render
    done();
  }
})
