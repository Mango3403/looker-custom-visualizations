import React, { useRef, useState, useEffect } from 'react';
import './ChartComponent.css';
import D3Component from './d3-component.js';
import { fetchData, getSVGString, svgString2Image } from './common/util';

let vis = null;
const WIDTH = 800;
const HEIGHT = 400;
const THEMES = {
  'default': {
    background: '#FFFFFF',
    title: '#666666',
    subTitle: '#666666',
    line: '#36C799',
    grid: '#e6e6e6',
    label: '#666666',
    tooltip: {
      line: '#000',
      text: '#000'
    }
  },
  'dark': {
    background: '#2a2a2b',
    title: '#E0E0E3',
    subTitle: '#E0E0E3',
    line: '#36C799',
    grid: '#707073',
    label: '#E0E0E3',
    tooltip: {
      line: '#fff',
      text: '#fff'
    }
  }
}
const CHART_TYPES = ['line', 'bar'];
const EXPORT_TYPES = ['png'];

export default function Chart({ title, subTitle, event_file, data, start_time, end_time }) {
  const scaleX = WIDTH / window.innerWidth;
  const scaleY = HEIGHT / window.innerHeight;
  const [width, setWidth] = useState(WIDTH);
  const [height, setHeight] = useState(HEIGHT);
  const [startTime, setStartTime] = useState(start_time);
  const [endTime, setEndTime] = useState(end_time);
  const [startTimeFormatted, setStartTimeFormatted] = useState('');
  const [endTimeFormatted, setEndTimeFormatted] = useState('');
  const [dataset, setDataset] = useState(data);
  const [file, setFile] = useState(event_file);
  const [lineTypes, setLineTypes] = useState([
    { title: 'curveLinear', curve: d3.curveLinear, checked: true },
    { title: 'curveStep', curve: d3.curveStep, checked: false },
  ]);
  const [theme, setTheme] = useState('default');
  const [exportType, setExportType] = useState('png');
  const [chartType, setChartType] = useState(['line', 'bar']);
  const chartRef = useRef();

  useEffect(handleResize, [])
  useEffect(() => {
    setStartTimeFormatted(formatTime(startTime));
  }, [startTime])
  useEffect(() => {
    setEndTimeFormatted(formatTime(endTime));
  }, [endTime])
  useEffect(() => {
    const d3Props = {
      data: dataset,
      width,
      height,
      background: THEMES[theme].background,
      title: { color: THEMES[theme].title, text: title },
      subTitle: { color: THEMES[theme].subTitle, text: subTitle },
      chartType: chartType,
      line: {
        color: THEMES[theme].line,
        width: 2,
        type: lineTypes.find(t => t.checked).curve
      },
      grid: { color: THEMES[theme].grid },
      label: { color: THEMES[theme].label },
      tooltip: {
        line: { color: THEMES[theme].tooltip.line },
        text: { color: THEMES[theme].tooltip.text }
      },
      animate: true,
      setStartTime: setStartTime,
      setEndTime: setEndTime,
    };
    vis = new D3Component(chartRef.current, d3Props);
    return () => destroy()
  }, [dataset, width, height, chartType, lineTypes, theme])

  function formatTime(d) {
    return d3.timeFormat('%Y-%m-%d %H %p')(d)
  }

  function parseTime(d) {
    return d3.timeParse('%Y-%m-%d %H %p')(d)
  }

  function handleResize() {
    let resizeTimer;
    const handleResize = () => {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function () {
        setWidth(window.innerWidth * scaleX);
        // setHeight(window.innerHeight * scaleY);
      }, 300);
    };
    window.addEventListener('resize', handleResize);

    return () => {
      vis = null;
      window.removeEventListener('resize', handleResize);
    };
  }

  function destroy() {
    vis = null;
    chartRef.current.innerHTML = '';
  }

  function handleReset() { vis && vis.resetChart(); }

  // function handleChangeFile() {
  //   fetchData(file).then(dataset => {
  //     setDataset(dataset);
  //     setStartTime(dataset[dataset.length - 1]['date']);
  //     setEndTime(dataset[0]['date']);
  //   });
  // }

  function handleExport() {
    const svgString = getSVGString(chartRef.current);
    svgString2Image(svgString, 2 * width, 2 * height, exportType, (dataBlob, filesize) => {
      saveAs(dataBlob, `D3 vis exported to PNG.${exportType}`); // FileSaver.js function
    }); // passes Blob and filesize String to the callback
  }

  return (
    <>
      <svg className="d3c" ref={chartRef} width={width} height={height}></svg>
      <div className="chart-handler">
        <div>
          <button type="button" onClick={handleReset}>reset</button>
        </div>
        <div>
          <span>Theme: </span>
          {Object.keys(THEMES).map(t => (
            <React.Fragment key={t}>
              <input type="radio" name="theme" value={t} checked={t == theme} onClick={(e) => setTheme(t)} />{t}
            </React.Fragment>
          ))}
        </div>
        <div>
          <span>Zoom to date: </span>
          <button type="button" onClick={() => {
            let d = new Date(endTime);
            d.setDate(d.getDate() - 1);
            if (vis) {
              setStartTime(d);
              vis.rangeDate(d, endTime);
            }
          }}>1d</button>
          <button type="button" onClick={() => {
            let d = new Date(endTime);
            d.setDate(d.getDate() - 3);
            if (vis) {
              setStartTime(d);
              vis.rangeDate(d, endTime);
            }
          }}>3d</button>
          <button type="button" onClick={handleReset}>all</button>
        </div>
        <div>
          <span>Date range: </span>
          <span>From </span>
          <input
            type="text"
            value={startTimeFormatted}
            onChange={(e) => {
              setStartTimeFormatted(e.target.value)
            }}
            onKeyPress={(e) => {
              if (e.key == 'Enter') {
                if (vis) {
                  const d = parseTime(startTimeFormatted);
                  setStartTime(d);
                  vis.rangeDate(d, endTime);
                }
              }
            }}
            onBlur={(e) => {
              if (vis) {
                const d = parseTime(startTimeFormatted);
                setStartTime(d);
                vis.rangeDate(d, endTime);
              }
            }}
          />
          <span> To </span>
          <input
            type="text"
            value={endTimeFormatted}
            onChange={(e) => {
              setEndTimeFormatted(e.target.value)
            }}
            onKeyPress={(e) => {
              if (e.key == 'Enter') {
                if (vis) {
                  const d = parseTime(endTimeFormatted);
                  setEndTime(d);
                  vis.rangeDate(startTime, d);
                }
              }
            }}
            onBlur={(e) => {
              if (vis) {
                const d = parseTime(endTimeFormatted);
                setEndTime(d);
                vis.rangeDate(startTime, d);
              }
            }}
          />
        </div>
        <div>
          <span>Line types: </span>
          {lineTypes.map(type => (
            <React.Fragment key={type.title}>
              <input type="radio" name="line-type" value={type.title} checked={type.checked} onClick={(e) => {
                setLineTypes(lineTypes.map(t => {
                  t.checked = t.title == e.target.value ? true : false
                  return t;
                }));
              }} />{type.title}
            </React.Fragment>
          ))}
        </div>
        <div>
          <span>Chart types: </span>
          {CHART_TYPES.map(type => (
            <React.Fragment key={type}>
              <input type="checkbox" name="chart-type" value={type} checked={chartType.indexOf(type) > -1} onClick={(e) => {
                let index = chartType.indexOf(type);
                if (index > -1) {
                  let arr = [...chartType];
                  arr.splice(index, 1);
                  setChartType(arr);
                } else {
                  setChartType([...chartType, type]);
                }
              }} />{type}
            </React.Fragment>
          ))}
        </div>
        {/* <div>
          <span>Import: </span>
          <select defaultValue={file} onChange={(e) => setFile(e.target.value)}>
            <option value="event_1">event_1</option>
            <option value="event_2">event_2</option>
            <option value="event_3">event_3</option>
          </select>
          <button type="button" onClick={handleChangeFile}>OK</button>
        </div> */}
        <div>
          <span>Export: </span>
          <select defaultValue={exportType} onChange={(e) => setExportType(e.target.value)}>
            {EXPORT_TYPES.map(type => <option key={type} value={type}>{type}</option>)}
          </select>
          <button type="button" onClick={handleExport}>OK</button>
        </div>
      </div>
    </>
  );
}