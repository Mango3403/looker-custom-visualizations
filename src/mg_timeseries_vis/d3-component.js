class D3Component {

  // containerEl;
  // props;
  // zoombrush;
  // width;
  // height;
  // svg: Selection<any>;
  // title: Selection<SVGTextElement>;
  // bisect;
  // x;
  // xAxis;
  // y;
  // yAxis;
  // clip;
  // focus;
  // brush;
  // zoom;
  // selection;
  // series;
  // series2;
  // context;

  constructor(containerEl, props) {
    this.containerEl = containerEl;
    this.props = props;
    this.zoombrush;
    const { width: WIDTH, height: HEIGHT, data, line, background, title, subTitle, grid, label, chartType, tooltip } = props;
    let margin = { top: 100, right: 20, bottom: 100, left: 20 };
    let margin2 = { top: 340, right: 20, bottom: 20, left: 20 };
    this.width = WIDTH - margin.left - margin.right;
    this.height = HEIGHT - margin.top - margin.bottom;
    this.height2 = HEIGHT - margin2.top - margin2.bottom;
    this.svg = d3.select(containerEl);

    this.svg
      .append('rect')
      .attr('fill', background)
      .attr('width', WIDTH)
      .attr('height', HEIGHT)

    this.svg
      .append('text')
      .attr('class', 'd3c-title')
      .attr('text-anchor', 'middle')
      .attr('x', this.width / 2)
      .attr('y', 38) // HACK: font size + margin top
      .style('color', title.color)
      .style('font-size', '18px')
      .style('fill', title.color)
      .append('tspan').html(title.text);

    this.svg
      .append('text')
      .attr('class', 'd3c-sub-title')
      .attr('text-anchor', 'middle')
      .attr('x', this.width / 2)
      .attr('y', 60)
      .style('color', subTitle.color)
      .style('font-size', '12px')
      .style('fill', subTitle.color)
      .append('tspan').html(subTitle.text);

    this.legend = this.svg
      .append('g')
      .attr('class', 'd3c-legend')
      .attr('transform', 'translate(' + margin.left + ',' + 72 + ')');

    this.legend.append('rect')
      .attr('class', 'd3c-legend-box')
      .attr('fill', 'none')
      .attr('x', 0)
      .attr('y', 0)
      .attr('rx', 0)
      .attr('ry', 0)
      .attr('width', 163)
      .attr('height', 30);

    this.legendGroup = this.legend.append('g');
    this.legendItem = this.legendGroup.append('g')
      .attr('class', 'd3c-legend-item')
    this.legendItem.append('path')
      .attr('d', 'M 0 11 L 16 11')
      .attr('stroke', line.color);
    this.legendItem.append('text')
      .html('value0')

    this.x = d3.scaleTime().range([0, this.width]);
    this.y = d3.scaleLinear().range([this.height, 0]);
    this.x2 = d3.scaleTime().range([0, this.width]);
    this.y2 = d3.scaleLinear().range([this.height2, 0]);
    this.xBand = d3
      .scaleBand()
      .domain(data.map(d => d.date))
      .range([0, this.width])
      .paddingInner(0.5);

    this.xAxis = d3.axisBottom(this.x).ticks(7).tickSize(-this.height);
    this.xAxis2 = d3.axisBottom(this.x2);
    this.yAxis = d3.axisLeft(this.y).ticks(4).tickSize(-this.width);

    this.brush = d3.brushX()                                   // Add the brush feature using the d3.brush function
      .extent([[0, 0], [this.width, this.height2]])            // initialise the brush series: start at 0,0 and finishes at width,height: it means I select the whole graph series
      .on("brush end", (event) => this.brushed(event))         // Each time the brush selection changes, trigger the 'updateChart' function

    this.zoom = d3.zoom()
      .scaleExtent([1, Infinity])
      .translateExtent([[0, 0], [this.width, this.height]])
      .extent([[0, 0], [this.width, this.height]])
      .on("zoom", (event) => this.zoomed(event));

    this.series = d3.line()
      .curve(line.type)
      .x((d) => { return this.x(d.date) })
      // .y0(this.height)
      .y((d) => { return this.y(+d.value0) })

    this.series2 = d3.line()
      .x((d) => { return this.x2(d.date) })
      // .y0(this.height2)
      .y((d) => { return this.y2(+d.value0) });

    this.clip = this.svg.append("defs").append("svg:clipPath")
      .attr("id", "d3c-clip")
      .append("svg:rect")
      .attr("width", this.width)
      .attr("height", this.height);

    this.focus = this.svg.append('g')
      .attr("class", "d3c-focus")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    this.context = this.svg.append("g")
      .attr("class", "d3c-context")
      .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

    // Render charts by data
    this.x.domain(d3.extent(data, function (d) { return d.date; }))
    this.y.domain([
      d3.min(data, function (d) { return +d.value0; }),
      d3.max(data, function (d) { return +d.value0; })
    ])
    this.x2.domain(this.x.domain())
    this.y2.domain(this.y.domain())

    let xTicks = this.focus.append("g")
      .attr("class", "d3c-axis d3c-axis--x")
      .attr("transform", "translate(0," + this.height + ")")
      .call(this.xAxis);
    xTicks.selectAll("text")
      .attr("dx", "-1.2em")
      .attr("dy", "1.2em")
      .attr("stroke", label.color)
      .attr("transform", "rotate(-45)");
    xTicks.selectAll("line")
      .attr("stroke", grid.color)
      .attr("stroke-width", "1px");

    let yTicks = this.focus.append("g")
      .attr('class', 'd3c-axis d3c-axis--y')
      .call(this.yAxis);
    yTicks.selectAll("text")
      .attr("stroke", label.color);
    yTicks.selectAll("line")
      .attr("stroke", grid.color)
      .attr("stroke-width", "1px");

    // Line
    this.focus.append("path").datum(data)
      .attr("class", "d3c-series")
      .style("clip-path", "url(#d3c-clip)")
      .attr("fill", "none")
      .attr("stroke", line.color)
      .attr("stroke-width", line.width)
      .attr("d", this.series);

    // TODO very slow
    // Bar
    chartType.indexOf('bar') > -1 && this.focus.append('g')
      .attr("class", "d3c-bars")
      .style("clip-path", "url(#d3c-clip)")
      .selectAll('rect')
      .data(data)
      .join(enter => {
        enter
          .filter(d => this.y(0) - this.y(d.value0) != 0)
          .append('rect')
          .style('fill', d => this.y(0) - this.y(d.value0) > 0 ? '#fd4d61' : '#8085e9')
          .attr('x', d => this.x(d.date) - this.xBand.bandwidth() / 2)
          .attr('y', d => this.y(0) - this.y(d.value0) > 0 ? this.y(d.value0) : this.y(0))
          .attr('height', d => Math.abs(this.y(0) - this.y(d.value0)))
          .attr('width', this.xBand.bandwidth());
      })

    this.context.append("path").datum(data)
      .attr("class", "d3c-series")
      .attr("fill", "none")
      .attr('stroke', line.color)
      .attr("stroke-width", line.width)
      .attr("d", this.series2);

    let xTicks2 = this.context.append("g")
      .attr("class", "d3c-axis d3c-axis--x")
      .attr("transform", "translate(0," + this.height2 + ")")
      .call(this.xAxis2);
    xTicks2.selectAll("text")
      .attr("stroke", label.color);

    this.context.append("g")
      .attr("class", "d3c-brush")
      .call(this.brush)
      .call(this.brush.move, this.x.range());

    const tip = new Tooltip(this.focus, {
      data: data,
      x: this.x,
      y: this.y,
      width: this.width,
      height: this.height,
      tooltip: tooltip
    });

    // Add zoom
    this.svg.append("rect")
      .attr("class", "d3c-zoom")
      .attr("width", this.width)
      .attr("height", this.height)
      .attr("fill", "none")
      .attr("pointer-events", "all")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .call(this.zoom)
      .on("mouseover", () => tip.show())
      .on("mouseout", () => tip.hide())
      .on("mousemove", (event) => tip.update(event))
    // .on("mousedown.zoom", null)
    // .on("touchstart.zoom", null)
    // .on("touchmove.zoom", null)
    // .on("touchend.zoom", null);
  }

  rangeDate(startTime, endTime) {
    if (this.zoombrush) return; // ignore brush-by-zoom
    this.zoombrush = 1;

    const { animate } = this.props;
    this.x.domain([startTime, endTime]);

    // lines update
    this.focus.select('.d3c-series')
      .transition().duration(animate ? 750 : 0)
      .attr("d", this.series);

    // bars update
    this.focus.selectAll(".d3c-bars rect")
      .attr("x", d => this.x(d.date) - this.xBand.bandwidth() / 2)
      .attr("width", this.xBand.bandwidth());

    // axis update
    this.focus.select('.d3c-axis--x')
      .transition().duration(animate ? 750 : 0)
      .call(this.xAxis);

    this.context.select(".d3c-brush")
      .transition().duration(animate ? 750 : 0)
      .call(this.brush.move, [startTime, endTime].map(this.x2));

    this.zoombrush = 0;
  }

  zoomed(event) {
    if (this.zoombrush) return; // ignore brush-by-zoom
    this.zoombrush = 1;

    const { setStartTime, setEndTime, label, grid } = this.props;
    const t = event.transform;
    this.x.domain(t.rescaleX(this.x2).domain());

    // lines update
    this.focus.select('.d3c-series').attr("d", this.series);

    // bars update
    this.xBand.range([0, t.applyX(this.width) - t.applyX(0)]);
    this.focus.selectAll(".d3c-bars rect")
      .attr("x", d => this.x(d.date) - this.xBand.bandwidth() / 2)
      .attr("width", this.xBand.bandwidth());

    // axis update
    let xTicks = this.focus.select('.d3c-axis--x').call(this.xAxis)
    xTicks.selectAll("text")
      .attr("dx", "-1.2em")
      .attr("dy", "1.2em")
      .attr("stroke", label.color)
      .attr("transform", "rotate(-45)");
    xTicks.selectAll("line")
      .attr("stroke", grid.color)
      .attr("stroke-width", "1px");

    this.context.select(".d3c-brush").call(this.brush.move, this.x.range().map(t.invertX, t));
    setStartTime(this.xAxis.scale().domain()[0])
    setEndTime(this.xAxis.scale().domain()[1])

    this.zoombrush = 0;
  }

  brushed(event) {
    if (this.zoombrush) return; // ignore brush-by-zoom
    this.zoombrush = 1;

    const { setStartTime, setEndTime, label, grid } = this.props;
    const s = event.selection || this.x2.range();
    this.x.domain(s.map(this.x2.invert, this.x2));

    // lines update
    this.focus.select(".d3c-series").attr("d", this.series);

    // bars update
    this.xBand.range([0, this.width * (this.width / (s[1] - s[0]))]);
    this.focus.selectAll(".d3c-bars rect")
      .attr("x", d => this.x(d.date) - this.xBand.bandwidth() / 2)
      .attr("width", this.xBand.bandwidth());

    // axis update
    let xTicks = this.focus.select(".d3c-axis--x").call(this.xAxis);
    xTicks.selectAll("text")
      .attr("dx", "-1.2em")
      .attr("dy", "1.2em")
      .attr("stroke", label.color)
      .attr("transform", "rotate(-45)");
    xTicks.selectAll("line")
      .attr("stroke", grid.color)
      .attr("stroke-width", "1px");

    this.svg.select(".d3c-zoom").call(this.zoom.transform, d3.zoomIdentity
      .scale(this.width / (s[1] - s[0]))
      .translate(-s[0], 0));
    setStartTime(this.xAxis.scale().domain()[0])
    setEndTime(this.xAxis.scale().domain()[1])

    this.zoombrush = 0;
  }

  resetChart() {
    const { data, animate, setStartTime, setEndTime } = this.props;
    const range = d3.extent(data, function (d) { return d.date; });
    this.x.domain(range);

    // axis update
    this.focus.select('.d3c-axis--x')
      .transition().duration(animate ? 750 : 0)
      .call(this.xAxis)
    this.focus.select('.d3c-series')
      .transition().duration(animate ? 750 : 0)
      .attr("d", this.series);

    // bars update
    this.xBand.range([0, this.width]);
    this.focus.selectAll(".d3c-bars rect")
      .attr("x", d => this.x(d.date) - this.xBand.bandwidth() / 2)
      .attr("width", this.xBand.bandwidth());

    // Reset zoom
    this.svg.select('.d3c-zoom')
      .transition().duration(animate ? 750 : 0)
      .call(this.zoom.transform, d3.zoomIdentity)
    setStartTime(range[0]);
    setEndTime(range[1]);
  }
}

class Tooltip {
  constructor(selection, props) {
    this.selection = selection;
    this.props = props;
    const { width, height, tooltip } = props;
    // this._date = svg`<text y="-22"></text>`;
    // this._value = svg`<text y="-12"></text>`;
    // this.node = svg`<g pointer-events="none" display="none" font-family="sans-serif" font-size="10" text-anchor="middle">
    //   <rect x="-27" width="54" y="-30" height="20" fill="white"></rect>
    //   ${this._date}
    //   ${this._value}
    //   <circle r="2.5"></circle>
    // </g>`;
    this.node = this.selection.append("g")
      .attr("class", "d3c-intersection")
      .style("display", "none");

    // append the circle at the intersection
    this.node.append("circle")
      .style("fill", "none")
      .style("stroke", tooltip.line.color)
      .attr("r", 4);

    // append the x line
    this.node.append("line")
      .attr("class", "x")
      .style("stroke", tooltip.line.color)
      .style("stroke-dasharray", "3,3")
      .style("opacity", 0.5)
      .attr("y1", 0)
      .attr("y2", height);

    // append the y line
    this.node.append("line")
      .attr("class", "y")
      .style("stroke", tooltip.line.color)
      .style("stroke-dasharray", "3,3")
      .style("opacity", 0.5)
      .attr("x1", width)
      .attr("x2", width);

    // place the value at the intersection
    this.node.append("text")
      .attr("class", "y1")
      .style("stroke", tooltip.text.color)
      .style("stroke-width", 0.1)
      .attr("dx", 8)
      .attr("dy", "-.3em");
    // this.node.append("text")
    //   .attr("class", "y2")
    //   .style("stroke", tooltip.text.color)
    //   .attr("dx", 8)
    //   .attr("dy", "-.3em");

    // // place the date at the intersection
    // this.node.append("text")
    //   .attr("class", "y3")
    //   .style("stroke", "#000")
    //   .style("stroke-width", "3.5px")
    //   .style("opacity", 0.8)
    //   .attr("dx", 8)
    //   .attr("dy", "1em");
    // this.node.append("text")
    //   .attr("class", "y4")
    //   .attr("dx", 8)
    //   .attr("dy", "1em");
  }
  formatDate(date) {
    return date.toLocaleString("en", {
      month: "short",
      day: "numeric",
      year: "numeric",
      timeZone: "UTC"
    });
  }
  formatValue(value) {
    return value.toFixed(3);
  }
  bisect(mx) {
    const { data, x } = this.props;
    const bisectDate = d3.bisector(function (d, x) {
      return x - d.date;
    }).left;
    const date = x.invert(mx);
    const i = bisectDate(data, date, 1);
    const d0 = data[i];
    const d1 = data[i - 1];
    return d1 && (date - d0.date > d1.date - date) ? d1 : d0;
  }
  show(d) {
    // this.node.removeAttribute("display");
    // this.node.setAttribute("transform", `translate(${x(d.date)},${y(d.close)})`);
    // this._date.textContent = this.formatDate(d.date);
    // this._value.textContent = d.close;
    this.node.style("display", null);
  }
  hide() {
    this.node.style("display", "none");
  }
  update(event) {
    const { date, value0: value } = this.bisect(d3.pointer(event)[0]);
    const { x, y, width, height } = this.props;

    this.node.select("circle")
      .transition().duration(50)
      .attr("transform", "translate(" + x(date) + "," + y(value) + ")");

    this.node.select("text.y1")
      .transition().duration(50)
      .attr("transform", "translate(" + x(date) + "," + y(value) + ")")
      .text(this.formatValue(value));

    // this.node.select("text.y2")
    //   .attr("transform", "translate(" + x(date) + "," + y(value) + ")")
    //   .text(value);

    // this.node.select("text.y3")
    //   .attr("transform", "translate(" + x(date) + "," + y(value) + ")")
    //   .text(this.formatDate(date));

    // this.node.select("text.y4")
    //   .attr("transform", "translate(" + x(date) + "," + y(value) + ")")
    //   .text(this.formatDate(date));

    this.node.select(".x")
      .transition().duration(50)
      .attr("transform", "translate(" + x(date) + "," + y(value) + ")")
      .attr("y2", height - y(value));

    this.node.select(".y")
      .transition().duration(50)
      .attr("transform", "translate(" + width * -1 + "," + y(value) + ")")
      .attr("x2", width + width);
  }
}

export default D3Component;