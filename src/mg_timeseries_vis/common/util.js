export function fetchData(event_file) {
  // Parse data
  const handler = [
    ({ data, config }) => {
      return data.map(function (item) {
        let output = {
          time_column: item[config.query_fields.dimensions[0].name]["value"]
        };

        for (let i = 1; i < config.query_fields.dimensions.length; i++) {

          let value = item[config.query_fields.dimensions[i].name]["value"];
          let label = config.query_fields.dimensions[i].label_short;

          output[label] = value;
        }

        return output;
      });
    },
    ({ data, config }) => {
      return data.map(function (item) {
        let output = {
          time_column: item[config.query_fields.dimensions[0].name]["value"]
        };

        for (let i = 0; i < config.query_fields.measures.length; i++) {

          let value = item[config.query_fields.measures[i].name]["value"];
          let label = config.query_fields.measures[i].label;

          output[label] = value;
        }

        return output;
      });
    },
    ({ data, config }) => {
      return data.map(function (item) {
        let output = {
          time_column: item[config.query_fields.dimensions[0].name]["value"]
        };

        for (let i = 0; i < config.query_fields.dimensions.length; i++) {

          let value = item[config.query_fields.measures[0].name]["value"];
          let label = config.query_fields.measures[0].label_short;

          output[label] = value;
        }

        return output;
      });
    },
  ]

  // HACK
  let parse_time;
  let parse_data;
  if (event_file === 'event_1') { parse_data = handler[0]; parse_time = d3.timeParse("%Y-%m-%d %H:%M:%S"); }
  else if (event_file === 'event_2') { parse_data = handler[1]; parse_time = (str) => new Date(str); }
  else if (event_file === 'event_3') { parse_data = handler[2]; parse_time = d3.timeParse("%Y-%m-%d"); }

  return new Promise((resolve, reject) => {
    d3.text('http://localhost:8000/event/mg_timeseries_vis/' + event_file)
      .then(file_content => {
        return new Promise(resolve => {
          const elem = document.createElement('script');
          elem.innerHTML = file_content;
          document.body.appendChild(elem);
          setTimeout(() => {
            resolve(globalThis.data);
          }, 200);
        })
      }).then(content => {
        if (content) {
          const parsed_data = parse_data({ ...content });
          console.log('parsed_data', parsed_data);
          const [time_column, ...traces] = Object.keys(parsed_data[0]);
          let dataset = [];
          if (traces.length > 0) {
            dataset = parsed_data.map(d => {
              return traces.reduce(
                (data, key, idx) => ({ ...data, ['value' + idx]: d[key] }),
                { date: parse_time(d['time_column']) }
              );
            });
          } else {
            dataset = parsed_data.map(d => ({ date: parse_time(d['time_column']) }))
          }

          resolve(dataset);
        }
      })
      .catch(error => this.addError({ title: 'Fetch Event File Failure', message: error }))
  })
}

// Source: https://jsfiddle.net/c19664p3/10/
// Below are the functions that handle actual exporting:
// getSVGString ( svgNode ) and svgString2Image( svgString, width, height, format, callback )
/**
 *
 * @param {SVGSVGElement} svgNode
 */
export function getSVGString(svgNode) {
  svgNode.setAttribute('xlink', 'http://www.w3.org/1999/xlink');
  var cssStyleText = getCSSStyles(svgNode);
  appendCSS(cssStyleText, svgNode);

  var serializer = new XMLSerializer();
  var svgString = serializer.serializeToString(svgNode);
  svgString = svgString.replace(/(\w+)?:?xlink=/g, 'xmlns:xlink='); // Fix root xlink without namespace
  svgString = svgString.replace(/NS\d+:href/g, 'xlink:href'); // Safari NS namespace fix

  return svgString;

  function getCSSStyles(parentElement) {
    var selectorTextArr = [];

    // Add Parent element Id and Classes to the list
    selectorTextArr.push('#' + parentElement.id);
    for (var c = 0; c < parentElement.classList.length; c++)
      if (!contains('.' + parentElement.classList[c], selectorTextArr))
        selectorTextArr.push('.' + parentElement.classList[c]);

    // Add Children element Ids and Classes to the list
    var nodes = parentElement.getElementsByTagName("*");
    for (var i = 0; i < nodes.length; i++) {
      var id = nodes[i].id;
      if (!contains('#' + id, selectorTextArr))
        selectorTextArr.push('#' + id);

      var classes = nodes[i].classList;
      for (var c = 0; c < classes.length; c++)
        if (!contains('.' + classes[c], selectorTextArr))
          selectorTextArr.push('.' + classes[c]);
    }

    // Extract CSS Rules
    var extractedCSSText = "";
    for (var i = 0; i < document.styleSheets.length; i++) {
      var s = document.styleSheets[i];

      try {
        if (!s.cssRules) continue;
      } catch (e) {
        if (e.name !== 'SecurityError') throw e; // for Firefox
        continue;
      }

      var cssRules = s.cssRules;
      for (var r = 0; r < cssRules.length; r++) {
        if (contains(cssRules[r].selectorText, selectorTextArr))
          extractedCSSText += cssRules[r].cssText;
      }
    }

    function contains(str, arr) {
      return arr.indexOf(str) === -1 ? false : true;
    }

    return extractedCSSText;
  }

  function appendCSS(cssText, element) {
    var styleElement = document.createElement("style");
    styleElement.setAttribute("type", "text/css");
    styleElement.innerHTML = cssText;
    var refNode = element.hasChildNodes() ? element.children[0] : null;
    element.insertBefore(styleElement, refNode);
  }
}

// TODO: add loading
/**
 *
 * @param {string} svgString
 * @param {number} width
 * @param {number} height
 * @param {'png' | 'jpeg'} format
 * @param {function} callback
 */
export function svgString2Image(svgString, width, height, format, callback) {
  var format = format ? format : 'png';

  var imgsrc = 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svgString))); // Convert SVG string to data URL

  var canvas = document.createElement("canvas");
  var context = canvas.getContext("2d");

  canvas.width = width;
  canvas.height = height;

  var image = new Image();
  image.onload = function () {
    context.clearRect(0, 0, width, height);
    context.drawImage(image, 0, 0, width, height);

    canvas.toBlob(function (blob) {
      var filesize = Math.round(blob.length / 1024) + ' KB';
      if (callback) callback(blob, filesize);
    });
  };

  image.src = imgsrc;
}
