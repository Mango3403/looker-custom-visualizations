import React, { useState, useEffect } from 'react';

// Create (or import) our react component
export default class Loading extends React.Component {
  constructor(props) {
    // So we have access to 'this'
    super(props)
  }

  // render our data
  render() {
    return <div id="myDiv">Loading...</div>;
  }
}
