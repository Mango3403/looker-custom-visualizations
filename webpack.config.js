var path = require('path')

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var webpackConfig = {
  mode: 'production',
  entry: {
    simple_vis: './src/simple_vis/hello_world_react.js',
    repeater: './src/repeater/repeater.js',
    timeseries_vis: './src/timeseries_vis/plot_timeseries.js',
    heatmap_vis: './src/heatmap_vis/plot_heatmap.js',
    // heatmap_api_vis: 'src/heatmap_api_vis/matrix.js',
    mg_helloworld: './src/mg_helloworld/hello.js',
    mg_timeseries_vis: './src/mg_timeseries_vis/chart.js'
  },
  output: {
    filename: "[name].js",
    path: path.join(__dirname, "dist"),
    library: "[name]",
    libraryTarget: "umd"
  },
  resolve: {
    extensions: [".ts", ".js"]
  },
  plugins: [
    new UglifyJSPlugin()
  ],
  module: {
    rules: [
      { test: /\.js$/, loader: "babel-loader" },
      { test: /\.ts$/, loader: "ts-loader" },
      { test: /\.css$/, loader: ['to-string-loader', 'css-loader'] }
    ]
  },
  stats: {
    warningsFilter: /export.*liquidfillgauge.*was not found/
  },
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  }
}

module.exports = webpackConfig
