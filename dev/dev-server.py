from flask import Flask, render_template, send_file
import json
from os import path

app = Flask(
    __name__,
    static_folder="../dist",
    static_url_path="/dist",
    template_folder="templates",
)


@app.route("/")
def index():
    return "INDEX"


@app.route("/favicon.ico")
def favicon():
    return send_file("favicon.ico")


@app.route("/view/<function_name>/<event_name>")
@app.route("/view/<function_name>")
def display_function(function_name, event_name="event_1"):
    return render_template(
        "view_tester.html", function_name=function_name, event_name=event_name
    )


@app.route("/event/<function_name>/<event_name>")
def fetch_event(function_name, event_name):
    with open(
        path.join("..", "src", function_name, "sample_data", f"{event_name}.json")
    ) as filein:
        data = filein.read()

    return f"var data = {data};"


if __name__ == "__main__":
    app.run(debug=True, port=8000, host="0.0.0.0")
